import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { GPlateformeSharedModule } from 'app/shared/shared.module';
import { GPlateformeCoreModule } from 'app/core/core.module';
import { GPlateformeAppRoutingModule } from './app-routing.module';
import { GPlateformeHomeModule } from './home/home.module';
import { GPlateformeEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    GPlateformeSharedModule,
    GPlateformeCoreModule,
    GPlateformeHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    GPlateformeEntityModule,
    GPlateformeAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class GPlateformeAppModule {}
